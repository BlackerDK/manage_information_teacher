﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
	public class Error
	{
		public Error() { }
		public static string ErrorString(string msg)
		{
			string data;
			do
			{
				Console.Write(msg);
				data = Console.ReadLine();
			} while (data.Length <= 0);
			return data;
		}
		public static int ErrorInt(string msg)
		{
			int data;
			string enter;
			bool error;
			do 
			{
				Console.Write(msg);
				enter = Console.ReadLine();
				error = int.TryParse(enter, out data);
				if (!error)
				{
					Console.WriteLine("Vui long nhap lai! ");
				}
			} while (!error);
			return data;
		}
		public static double ErrorDouble(string msg)
		{
			double data;
			string enter;
			bool error;
			do
			{
				Console.Write(msg);
				enter = Console.ReadLine();
				error = double.TryParse(enter, out data);
				if (!error)
				{
					Console.WriteLine("Vui long nhap lai! ");
				}
			} while (!error);
			return data;
		}
	}
}
