﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
	public class GiaoVien : NguoiLaoDong
	{
		public double HeSoLuong { get; set; }
        public GiaoVien()
        {
			
        }
		public GiaoVien(string hoten, int namsinh, double luongcoban, double hesoluong) : base(hoten, namsinh, luongcoban)
		{
			 HeSoLuong = hesoluong;
		}
		public void NhapThongTin(double hesoluong) 
		{
			hesoluong = HeSoLuong;
		}
		public override double TinhLuong()
		{
			return HeSoLuong * LuongCoBan * 1.25 ;
		}
		public new void XuatThongTin()
		{
			base.XuatThongTin();
			Console.Write(" He So Luong: " + HeSoLuong);
			Console.Write(" Tong Luong: "+TinhLuong());
		}
		public double XuLy()
		{
			return HeSoLuong + 0.6;		
		}
	}
}
