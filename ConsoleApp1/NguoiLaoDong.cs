﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
	public class NguoiLaoDong
	{
		public string HoTen { get; set; }
		public int NamSinh { get; set; }
		public double LuongCoBan { get; set; }
		public NguoiLaoDong()
		{
		}
		public NguoiLaoDong(string hoten, int namsinh, double luongcoban)
		{
			this.HoTen = hoten;
			this.NamSinh = namsinh;
			this.LuongCoBan = luongcoban;
		}
		public void NhapThongTin(string hoten, int namsinh, double luongcoban)
		{
			hoten = HoTen;
			namsinh = NamSinh;
			luongcoban = LuongCoBan;
		}
		public virtual double TinhLuong()
		{
			return LuongCoBan;
		}
		public void XuatThongTin()
		{
			Console.WriteLine($"Ho Ten: {HoTen}, Nam Sinh: {NamSinh}, LuongCoBan: {LuongCoBan}");
		}
	}
}
