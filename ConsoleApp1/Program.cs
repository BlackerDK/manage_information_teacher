﻿using ConsoleApp1;
using System;
namespace Loop
{
	class Program
	{
		static void Main(string[] args)
		{
			//1 create listGiaovien
			List<GiaoVien> listGiaoVien = new List<GiaoVien>();
			//2 Enter infor of GiaoVien
			int quantity =Error.ErrorInt("Nhap so luong Nguoi Lao Dong: ");
			for (int i = 1; i <= quantity; i++)
			{
				Console.WriteLine("Giao vien: "+i);
				string hoten = Error.ErrorString("Nhap Ten Giao Vien: ");
				int namsinh = Error.ErrorInt("Nhap Nam Sinh: ");
				double luongcoban =Error.ErrorDouble("Luong Co Ban: ");
				double hesoluong = Error.ErrorDouble("He So Luong: ");
				listGiaoVien.Add(new GiaoVien(hoten, namsinh, luongcoban, hesoluong));
			}
			GiaoVien GvLuongThap = listGiaoVien.OrderBy(p=>p.TinhLuong()).FirstOrDefault();
			if (GvLuongThap != null)
			{
			GvLuongThap.XuatThongTin();
			}
			else { Console.WriteLine("Null"); }
			Console.WriteLine("Thoat Program !");
			Console.Read();
		}
	}
}
